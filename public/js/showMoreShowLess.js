class ShowMoreShowLess {

	constructor(elementClass, btn, btnTextMore, btnTextLess, startIndex, nbElementsToShow)
	{
		let nbOfElement		= nbElementsToShow;
		let elementLength 	= $(elementClass).length;
		let index 			= startIndex;
		let button			= btn;

		$(elementClass).hide();
		$(elementClass+':lt('+index+')').show();

		if (index == elementLength){
			$(button).hide();
		}

		$(button).click(function () {
	    	let type = $(this).attr('data-type');

	    	if (type == '+'){
	    		index = (index + nbOfElement <= elementLength) ? index + nbOfElement : elementLength;
	        	console.log(index);
	        	$(elementClass+':lt('+index+')').show();
	        	if(index == elementLength){
	        		$(button).attr('data-type', '-');
	        		$(button).html(btnTextLess);
	        		index = 2;
	        	}
	    	} else {
	        	$(elementClass).not(':lt('+startIndex+')').hide();
	        	$(button).attr('data-type', '+');
	        	$(button).html(btnTextMore);
	    	}
	    });
	}
}
