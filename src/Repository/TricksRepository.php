<?php

namespace App\Repository;

use App\Entity\Tricks;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Tricks|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tricks|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tricks[]    findAll()
 * @method Tricks[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TricksRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tricks::class);
    }

    // /**
    //  * @return Tricks[] Returns an array of Tricks objects
    //  */

    public function getTrick($value): array
    {
        return $this->createQueryBuilder('t')
            ->select('t','u')
            ->innerJoin('t.fk_author', 'u','WITH','t.fk_author = u.id')
            ->andWhere('t.slug = :slug')
            ->setParameter('slug', $value)
            /*->orderBy('c.id', 'ASC')
            ->setMaxResults(10)*/
            ->getQuery()
            ->getResult();

    }

    public function getTrickById($id): array
    {
        return $this->createQueryBuilder('t')
            ->select('t','u')
            ->innerJoin('t.fk_author', 'u','WITH','t.fk_author = u.id')
            ->andWhere('t.id = :id')
            ->setParameter('id', $id)
            /*->orderBy('c.id', 'ASC')
            ->setMaxResults(10)*/
            ->getQuery()
            ->getResult();

    }

}
