<?php

namespace App\Repository;

use App\Entity\CategoryRef;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CategoryRef|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoryRef|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoryRef[]    findAll()
 * @method CategoryRef[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoryRefRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryRef::class);
    }

    // /**
    //  * @return CategoryRef[] Returns an array of CategoryRef objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CategoryRef
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
