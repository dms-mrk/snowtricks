<?php

namespace App\Entity;

use App\Repository\TricksRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Validator\Constraints as TricksAssert;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * @ORM\Entity(repositoryClass=TricksRepository::class)
 * @UniqueEntity(fields="title", message="Ce trick existe déjà dans la base de données.")
 */
class Tricks
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     * @Assert\NotBlank(message = "Le champ ne doit pas être vide")
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Le nom de la figure doit contenir au moins {{ limit }} caractères",
     *      maxMessage = "Le nom de la figure doit contenir au maximum {{ limit }} caractères",
     *      allowEmptyString = false
     * )
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(message = "Le champ ne doit pas être vide")
     */
    private $description;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_c;

    /**
     * @ORM\Column(type="datetime")
     */
    private $last_modification;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $pictures;

    /**
     * @ORM\Column(type="integer")
     */
    private $last_user_modification;


    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $fk_author;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $videos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cover;

    protected $tags;

    /**
     * @ORM\ManyToOne(targetEntity=CategoryRef::class, inversedBy="tricks")
     * @ORM\JoinColumn(nullable=false)
     */
    private $fk_group;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message = "Le champ ne doit pas être vide")
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $difficulty;

    /**
     * @ORM\OneToMany(targetEntity=Comments::class, mappedBy="fk_trick", orphanRemoval=true)
     */
    private $comments;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"title"})
     */
    private $slug;


    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateC(): ?\DateTimeInterface
    {
        return $this->date_c;
    }

    public function setDateC(\DateTimeInterface $date_c): self
    {
        $this->date_c = $date_c;

        return $this;
    }

    public function getLastModification(): ?\DateTimeInterface
    {
        return $this->last_modification;
    }

    public function setLastModification(\DateTimeInterface $last_modification): self
    {
        $this->last_modification = $last_modification;

        return $this;
    }

    public function getPictures()
    {
        return $this->pictures;
    }

    public function setPictures($pictures)
    {
        $this->pictures = $pictures;

        return $this;
    }

    public function getLastUserModification(): ?int
    {
        return $this->last_user_modification;
    }

    public function setLastUserModification(int $last_user_modification): self
    {
        $this->last_user_modification = $last_user_modification;

        return $this;
    }


    public function getFkAuthor(): ?User
    {
        return $this->fk_author;
    }

    public function setFkAuthor(?User $fk_author): self
    {
        $this->fk_author = $fk_author;

        return $this;
    }


    public function getVideos(): ?string
    {
        return $this->videos;
    }

    public function setVideos(?string $videos): self
    {
        $this->videos = $videos;

        return $this;
    }


    public function getCover(): ?string
    {
        return $this->cover;
    }

    public function setCover(string $cover): self
    {
        $this->cover = $cover;

        return $this;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getFkGroup(): ?CategoryRef
    {
        return $this->fk_group;
    }

    public function setFkGroup(?CategoryRef $fk_group): self
    {
        $this->fk_group = $fk_group;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getDifficulty(): ?string
    {
        return $this->difficulty;
    }

    public function setDifficulty(string $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection|Comments[]
     */

    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comments $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setFkTrick($this);
        }

        return $this;
    }

    public function removeComment(Comments $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getFkTrick() === $this) {
                $comment->setFkTrick(null);
            }
        }

        return $this;
    }


}
