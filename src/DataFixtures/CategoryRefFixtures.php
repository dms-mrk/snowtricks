<?php


namespace App\DataFixtures;

use App\Entity\Comments;
use App\Entity\Tricks;
use App\Entity\User;
use App\Entity\CategoryRef;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CategoryRefFixtures extends Fixture
{

    Const NB_GROUP_MAX = 10;

    public function load(ObjectManager $manager)
    {
        for ($i = 1; $i <= self::NB_GROUP_MAX; $i++ )
        {

            $category = new CategoryRef();
            $category->setName("Category" . $i);


            $manager->persist($category);
            $this->addReference('Category'.$i, $category);
        }

        $manager->flush();
    }

}
