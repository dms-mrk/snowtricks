<?php


namespace App\DataFixtures;

use App\Entity\Comments;
use App\Entity\Tricks;
use App\Entity\User;
use App\Entity\CategoryRef;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class TricksFixtures extends Fixture implements DependentFixtureInterface
{

    Const NB_TRICK_MAX = 10;
    private Const NB_VIDEO_MAX = 5;
    private Const POSITION = array("Regular","Goofy");
    private Const VIDEOS = array(
        "https://www.youtube.com/embed/SFYYzy0UF-8",
        "https://www.youtube.com/embed/eVCPycN00z0",
        "https://www.youtube.com/embed/V9xuy-rVj9w",
        "https://www.youtube.com/embed/uQgslXubZ4o",
        "https://www.youtube.com/embed/upbfb3Tz0F8",
        "https://www.youtube.com/embed/cYTf6YBNWro",
        "https://www.youtube.com/embed/7a0hbT0QtSw",
    );

    private $dirroot;

    public function __construct(UserPasswordEncoderInterface $encoder, KernelInterface $kernel)
    {
        $this->encoder = $encoder;
        $this->dirroot = $kernel->getProjectDir()."/public/uploads";
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create("fr-FR");

        for($i = 1; $i <= self::NB_TRICK_MAX; $i++)
        {
            $trick = new Tricks();
            $timezone = new \DateTimeZone('Europe/Paris');
            $time = new \DateTime('now', $timezone);
            $finder = new Finder();
            $rand_nb_video = rand(0,self::NB_VIDEO_MAX);
            $video = [];


            //TODO generer une methode pour les images

            for($j = 1; $j <= $rand_nb_video; $j++)
            {
                $video[] = self::VIDEOS[rand(0,count(self::VIDEOS)-1)];
            }
            $Videos = serialize($video);

            $user_ref_rand = rand(1,UserFixtures::NB_USER_MAX);
            $currentUser = $this->getReference("User".$user_ref_rand);

            //TODO generer une methode pour les images
            $rand_picture = $finder->files()->in($this->dirroot);

            foreach ($rand_picture as $file)
            {
                $Path[] = $file->getRelativePathname();
            }

            $lipsum = simplexml_load_file('http://www.lipsum.com/feed/xml?amount=1&what=paras&start=0')->lipsum;

            $trick->setFkAuthor($currentUser);
            $trick->setFkGroup($this->getReference("Category".rand(1,CategoryRefFixtures::NB_GROUP_MAX)));
            $trick->setTitle("Titre" . $i);
            $trick->setDescription($lipsum);
            $trick->setDateC($time);
            $trick->setLastModification($time);
            $trick->setLastUserModification($user_ref_rand);
            $trick->setVideos($Videos);
            $trick->setCover($Path[rand(0,count($Path)-1)]);
            $trick->setPosition(self::POSITION[rand(0,count(self::POSITION)-1)]);
            $trick->setDifficulty(rand(1,3));
            $trick->setSlug("Titre" . $i);
            //TODO ajouter setPicture

            $manager->persist($trick);
            $this->addReference('Trick'.$i, $trick);
        }

        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoryRefFixtures::class,
        );
    }
}
