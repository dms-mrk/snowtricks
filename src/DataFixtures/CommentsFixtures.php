<?php


namespace App\DataFixtures;

use App\Entity\Comments;
use App\Entity\Tricks;
use App\Entity\User;
use App\Entity\CategoryRef;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker;

class CommentsFixtures extends Fixture implements DependentFixtureInterface
{
    const NB_COMMENTS_MAX = 150;

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create("fr-FR");

        for ($i = 1; $i <= self::NB_COMMENTS_MAX; $i++)
        {
            $comments = new Comments();
            $comments->setFkUser($this->getReference("User".rand(1,UserFixtures::NB_USER_MAX)));
            $comments->setFkTrick($this->getReference("Trick".rand(1,TricksFixtures::NB_TRICK_MAX)));
            $comments->setDateC($faker->dateTime);
            $comments->setContent($faker->realText(50));

            $manager->persist($comments);
        }

        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
            CategoryRefFixtures::class,
            TricksFixtures::class,
        );
    }

}
