<?php


namespace App\DataFixtures;

use App\Entity\Comments;
use App\Entity\Tricks;
use App\Entity\User;
use App\Entity\CategoryRef;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Contracts\Service\ServiceSubscriberInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Faker;

class UserFixtures extends Fixture
{

    const NB_USER_MAX = 10;
    private const USER = 10;
    private $list_mail = ["gmail","live","outlook","yahoo"];
    private $list_photo = ["gmail","live","outlook","yahoo"];
    private $civility = ["Male","Female"];
    private $count;
    private $encoder;
    private $dirroot;

    public function __construct(UserPasswordEncoderInterface $encoder, KernelInterface $kernel)
    {
        $this->encoder = $encoder;
        $this->dirroot = $kernel->getProjectDir()."/public/uploads";
    }

    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create("fr-FR");
        $count_mails = count($this->list_mail);
        $finder = new Finder();
        $timezone = new \DateTimeZone('Europe/Paris');
        $rand_picture = $finder->files()->in($this->dirroot);

        foreach ($rand_picture as $file)
        {
            $Path[] = $file->getRelativePathname();
        }
        $rand_picture_count = count($rand_picture);

        for ($i = 1; $i <= self::NB_USER_MAX; $i++)
        {
            $civility = $this->civility[rand(0,count($this->civility)-1)];
            $firstname = $faker->firstName;
            $lastname = $faker->lastname;

            $user = new User();
            $user->setEmail($lastname . "." . $firstname."@" . $this->list_mail[rand(0,$count_mails-1)] . ".fr");
            $password = $this->encoder->encodePassword($user, 'pass_'.$i);
            $user->setPassword($password);
            $user->setRoles(["ROLE_USER"]);
            $user->setFirstname($firstname);
            $user->setLastname($lastname);
            //$user->setPicture($Path[rand(0,$rand_picture_count-1)]);
            $user->setDateC($faker->dateTime);

            $manager->persist($user);
            $this->addReference('User'.$i, $user);
        }

        $manager->flush();
    }

}
