<?php

namespace App\Form;

use App\Entity\Comments;
use App\Entity\Tricks;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CommentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('date_c')
            ->add('content', TextareaType::class,[
                'data_class' => null,
                'mapped' => false,
               // 'choice_label' => 'content',
            ])
            //->add('fk_trick')
            //->add('fk_user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            array('data_class' => null)
        ]);
    }
}
