<?php

namespace App\Form;

use App\Entity\Tricks;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use App\Entity\CategoryRef;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Validator\Constraints\NotBlank;


class TricksType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $group = new CategoryRef();
        $builder
            ->add('title', TextType::class, [
                'label' => false,
            ])
            ->add('description', TextareaType::class, [
                'label' => false,
            ])
            ->add('deleteOfPic', HiddenType::class, [
                'mapped' => false,
                'required' => false,
            ])
            ->add('deleteOfVid', HiddenType::class,[
                'mapped' => false,
                'required' => false
            ])
            ->add('cover', FileType::class,[
                'label' => false,
                'multiple' => false,
                'required' => false,
                'mapped' => false,
            /* 'constraints' => [
                 new File([
                     'maxSize' => '1024k',
                     'mimeTypes' => [
                         'jpeg',
                         'jpg',
                         'image/png',
                         //'application/x-pdf',
                     ],
                     'mimeTypesMessage' => 'Vous devez entrez un type de fichier valide (jpeg,jpg,png)',
                 ])
             ],*/
            // 'data_class' => null

         ])
            ->add('pictures', FileType::class,[
                'label' => false,
                'multiple' => true,
                'required' => false,
                'mapped' => false,
               /* 'constraints' => [
                    new File([
                        'maxSize' => '1024k',
                        'mimeTypes' => [
                            //'image/jpeg',
                            //'image/jpg',
                            'image/png',
                            //'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Vous devez entrez un type de fichier valide (jpeg,jpg,png)',
                    ])
                ],*/

            ])

            ->add('last_user_modification', HiddenType::class)
            ->add('fk_group', EntityType::class, [
                'label' => false,
                'class' => CategoryRef::class,
                'mapped' => true,
                'choice_label' => 'name',
                'choice_value' => 'id'
            ])
            ->add('videos', HiddenType::class,[
                //'label' => 'Videos',
                'data' => '',
            ])
            /*->add('tags', CollectionType::class, [
                'entry_type' => CategoriesType::class,
                'entry_options' => ['label' => false],
            ])*/
            ->add('position', TextType::class, [
                'label' => false,
                'mapped' => true,
                'required' => true,
            ])
            ->add('difficulty', ChoiceType::class, [
                'label' => false,
                'mapped' => true,
                'required' => true,
                'choices'  => [
                    'Facile' => 1,
                    'Moyen' => 2,
                    'Difficile' => 3,
                ],
            ]);

    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Tricks::class,
        ]);
    }
}
