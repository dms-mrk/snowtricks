<?php


namespace App\Validator\Constraints;


use Gedmo\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsAvailableExtensionValidator extends ConstraintValidator
{

    public function validate($file, Constraint $constraint)
    {
        //var_dump($fileExtension);exit;
        $match = substr($file,-5);
//dump($match);exit;
        if(!$match)
        {
            $this->context->buildViolation($constraint->message)
            ->setParameter("{{ text }}",$file)
                ->addViolation();
            //throw new UnexpectedValueException($fileExtension,'string');
        }
    }

}
