<?php

namespace App\Controller;

use App\Entity\Comments;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CommentsController extends AbstractController
{
    /**
     * @Route("/comments", name="comments")
     */
    public function index()
    {

        return $this->render('comments/index.html.twig', [
            'controller_name' => 'CommentsController',
        ]);
    }

    /**
     * @Route("/{id}", name="comments_delete", methods={"DELETE"})

     */
    public function deleteComments(Request $request, Comments $comments): Response
    {

        if ($this->isCsrfTokenValid('delete'.$comments->getId(), $request->request->get('_token')))
        {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($comments);
            $entityManager->flush();
        }

        $this->addFlash('success', 'Commentaire supprimé avec succès');
        return $this->redirectToRoute('home');
    }

}
