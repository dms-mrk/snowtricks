<?php

namespace App\Controller;

use App\Form\RegistrationType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Mime\Email;


class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/deconnexion", name="app_logout")
     */
    public function logout()
    {
        //throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
        //$this->get('session')->getFlashBag()->add('success', 'Vous êtes désormais déconnecté');

        return $this->render('home/index.html.twig');
    }

    /**
     * @Route("/inscription", name="security_registration")
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \Exception
     */
    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $passwordEncoder , MailerInterface $mailer)
    {
        
        $user = new User();
        $form = $this->createForm(RegistrationType::class, $user);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {

            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $user->setPassword($password);
            $roles[] = 'ROLE_USER';
            $user->setRoles($roles);

            $timezone = new \DateTimeZone('Europe/Paris');
            $user->setDateC(new \DateTime('now',$timezone));
            $manager->persist($user);
            $manager->flush();

            $email = (new Email())
                ->from('devmariko7@gmail.com')
                ->to('mariko-mamadou@outlook.fr')
                //->cc('cc@example.com')
                //->bcc('bcc@example.com')
                //->replyTo('fabien@example.com')
                //->priority(Email::PRIORITY_HIGH)
                ->subject('Nouvel Inscription!')
                ->text('Sending emails is fun again!')
                ->html('<p>Un nouvel utilisateur est inscrit sur votre site !</p>');

            $mailer->send($email);

            $this->addFlash('success', 'Votre êtes désormais inscrit sur snowtricks!');
            return $this->redirectToRoute('home');
        }

        if($form->isSubmitted() && $form->isValid() == false)
        {
            $this->addFlash('warning', 'Des erreurs sont présentes dans le formulaire!');
        }

        return $this->render('security/registration.html.twig', [
            'form'=> $form->createView()
        ]);
    }
}
