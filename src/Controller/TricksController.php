<?php

namespace App\Controller;

use App\Controller\handler\tricks\HandlerEditTrick;
use App\Controller\handler\tricks\HandlerNewTrick;
use App\Controller\handler\tricks\HandlerShowTrick;
use App\Entity\CategoryRef;
use App\Entity\Tricks;
use App\Entity\User;
use App\Entity\Comments;
use App\Form\TricksType;
use App\Form\CommentsType;
use App\Repository\TricksRepository;
use App\Repository\CategoryRefRepository;
use App\Repository\CommentsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
//require __DIR__.'/vendor/autoload.php';
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Twig\Cache\CacheInterface;


/**
 * @Route("/figure")
 */

class TricksController extends AbstractController
{
    /**
     * @Route("/", name="tricks_index", methods={"GET"})
     */
    public function index(TricksRepository $tricksRepository): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        return $this->render('tricks/index.html.twig', [
            'tricks' => $tricksRepository->findAll(),
        ]);
    }

    /**
     * @Route("/creation", name="tricks_new", methods={"GET","POST"})
     */
    public function new(Request $request, SluggerInterface $slugger, ValidatorInterface $validator): Response
    {

        $session = $this->getUser();
        $trick = new Tricks();

        $form = $this->createForm(TricksType::class, $trick);
        $submit = new HandlerNewTrick();
        $submit->setTrick($trick);
        $submit->setForm($form);
        $submit->setRequest($request);
        $form->handleRequest($request);

        if($form->isSubmitted())
        {
            $submit->setForm($form);
            $submit->setValidator($validator);
            $submit->setSlugger($slugger);
            $submit->setUploadDir($this->getParameter('files_directory'));
            $checks = $submit->myCheckForm();

            if($submit->SubmitForm($session))
            {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($submit->getTrick());
                $entityManager->flush();
                $this->addFlash('success', 'Figure crée avec succès!');
                return $this->redirectToRoute('tricks_index');

            }else{

                if(!empty($checks['errorMessage']))
                {
                    $this->addFlash('warning', $checks['errorMessage']);
                }
                else{
                    $this->addFlash('warning', 'Certains champs comportent des erreurs');
                }

            }
        }

            $olds_pictures = [];
            $olds_videos = [];
            $olds_cate = array('position'=>'','difficulty'=>'');

        return $this->render('tricks/new.html.twig', [
            'trick' => $trick,
            'form' => $form->createView(),
            'session' => $session,
            'olds_pictures' => $olds_pictures,
            'olds_videos' => $olds_videos,
            //'errors' => $errors,
        ]);
    }


// ================
    /**
     * @Route("/{slug}", name="tricks_show", methods={"GET","POST"})
     */
    public function show(Request $request, Tricks $trick, $slug, PaginatorInterface $paginator): Response
    {

        $comments = new Comments();
        $form = $this->createForm(CommentsType::class, $request);

        $tricks = $this->getDoctrine()
            ->getRepository(Tricks::class)
            ->getTrick($slug);

        $submit = new HandlerShowTrick();
        $submit->setTrick($tricks);
        $allMedias = $submit->getAllMedias();

        $trick_id = $tricks[0]->getId();

        $idGroup = $trick->getFkGroup()->getId();

        $categorieFigure = $this->getDoctrine()
            ->getRepository(CategoryRef::class)
            ->find($idGroup);

        $lesCommentaires = $this->getDoctrine()
            ->getRepository(Comments::class)
            ->getAllComments($trick_id);


        $AllComments = $paginator->paginate(
            $lesCommentaires, // on passe les données
            $request->query->getInt('page',1),
            10 // nombre d'élement par page
        );
        $AllComments->setTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig');
        $AllComments->setSortableTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_font_awesome_sortable_link.html.twig');

        $form->handleRequest($request);

        $erreur = false;
        //verification si champs commentaire est non vide
        if($form->isSubmitted() && empty($form->get('content')->getData()))
        {
            $erreur = true;
            $this->addFlash('warning', 'Le champs commentaire ne dois pas être vide');
        }

        if ($form->isSubmitted() && $form->isValid() && $erreur === false)
        {
            // auto recuperation des valeurs automatique
            //date_c
            $timezone = new \DateTimeZone('Europe/Paris');
            $comments->setDateC(new \DateTime('now', $timezone));
            //fk_trick
            $comments->setFkTrick($trick);
            //fk_user
            $user = $this->getUser();
            $comments->setFkUser($user);
            // content
            $getcommentaire = $form->get('content')->getData();
            $comments->setContent($getcommentaire);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comments);
            $entityManager->flush();

            $this->addFlash('success', 'Commentaire ajouté avec succès');
        }

        $olds_cate = "";

        return $this->render('tricks/show.html.twig', [
            'tricks' => $tricks,
            'comments' => $AllComments,
            'form' => $form->createView(),
            'allPictures' => $allMedias['allPictures'],
            'allVideos' => $allMedias['allVideos'],
            //'Allpics' => $Allpictures
            'olds_cate' => $olds_cate,
            'categorieFigure' => $categorieFigure
        ]);
    }

    /**
     * @Route("/{id}/modification", name="tricks_edit", methods={"GET","POST"})
     */

    public function edit(Request $request, Tricks $trick,$id, SluggerInterface $slugger, ValidatorInterface $validator): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

        $session = $this->getUser();
        $form = $this->createForm(TricksType::class, $trick);
        $submit = new HandlerEditTrick();
        $submit->setTrick($trick);
        $submit->setRequest($request);
        $submit->setForm($form);

        // RECUPERATION valeurs par defaults
        $olds = $this->getDoctrine()
            ->getRepository(Tricks::class)
            ->find($id);

        $datamedias = $submit->data_init($olds);
        $errors = $validator->validate($form);
        //dump($request);exit;
        $form->handleRequest($request);

        //if submit form
        if($form->isSubmitted())
        {
            $submit->setValidator($validator);
            $submit->setSlugger($slugger);
            $submit->setUploadDir($this->getParameter('files_directory'));

            $checks = $submit->myCheckForm();
            $submitStatus = $submit->SubmitForm($datamedias,$session);


            if ($submitStatus)
            {
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'Figure modifié avec succès!');
                return $this->redirectToRoute('tricks_index');

            }else{
                if(!empty($checks['errorMessage']))
                {
                    $this->addFlash('warning', $checks['errorMessage']);
                }
                else{
                    $this->addFlash('warning', 'Certains champs comportent des erreurs');
                }
            }
        }

            if(empty($olds_pictures))
            {
                $olds_pictures = [];
            }


        return $this->render('tricks/edit.html.twig', [
            'trick' => $trick,
            'form' => $form->createView(),
            'olds_pictures' => $datamedias['olds_pictures'],
            'olds_videos' => $datamedias['olds_videosView'],
            'errors' => $errors,
        ]);

    }


    /**
     * @Route("/{id}", name="tricks_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Tricks $trick): Response
    {
        if ($this->isCsrfTokenValid('delete'.$trick->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($trick);
            $entityManager->flush();
        }

        $this->addFlash('success', 'Figure supprimé avec succès');
        return $this->redirectToRoute('home');
    }



}
