<?php


namespace App\Controller\handler\tricks;


use App\Entity\Tricks;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;

class HandlerEditTrick extends AbstractController
{
    private $form;
    private $request;
    private $slugger;
    private $validator;
    private $trick;
    private $uploaddir;
    private $olds;

    public function setForm($form)
    {
        $this->form = $form;
    }

    public function getForm()
    {
        return $this->form;
    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setTrick($trick)
    {
        $this->trick = $trick;
    }

    public function getTrick()
    {
        return $this->trick;
    }

    public function setSlugger($slugger)
    {
        $this->slugger = $slugger;
    }

    public function getSlugger()
    {
        return $this->slugger;
    }

    public function setValidator($validator)
    {
        $this->slugger = $validator;
    }

    public function getValidator()
    {
        return $this->validator;
    }

    public function setUploadDir($uploaddir)
    {
        return $this->uploaddir = $uploaddir;
    }

    public function data_init($olds)
    {

        $this->olds = $olds;

        // un tableau des anciennes photos
        $mediasIndatabase = $this->getMediasIndatabse($this->olds->getPictures(),$this->olds->getVideos());

        foreach ($mediasIndatabase as $key => $typeOfmedia)
        {
            $olds_pictures = $mediasIndatabase["olds_pictures"];
            $olds_videos = $mediasIndatabase["olds_videos"];
            $olds_videosView = $mediasIndatabase["olds_videosView"];
        }

        return $array = array('olds_pictures'=>$olds_pictures,'olds_videos'=>$olds_videos,'olds_videosView'=>$olds_videosView);

    }

    public function myCheckForm()
    {

        $erreur = false;
        $message = "";
        $extensions = array("jpg","png","jpeg");
        $verifCover = $this->form->get('cover')->getData();
        $pictures = $this->form->get('pictures')->getData();


        if(!empty($verifCover) || $verifCover !== null)
        {

            if(!in_array(strtolower($verifCover->guessExtension()),$extensions))
            {
                $erreur = true;
                $message ="L'extension n'est pas valide. veuillez choisir une extension valide parmi : png, jpg, jpeg";
            }

            if($verifCover->getSize() > 2000000)
            {
                $erreur = true;
                $message = "warning', 'La taille du fichier est trop grande. la limite est de 2 mo";
            }
        }

        if($pictures !== NULL || !empty($pictures))
        {
            foreach ($pictures as $file)
            {
                if(!in_array(strtolower($file->guessExtension()),$extensions))
                {
                    $erreur = true;
                    $message ="Une extension n'est pas valide. veuillez choisir une extension valide parmi : png, jpg, jpeg";
                }

                if($file->getSize() > 2000000)
                {
                    $erreur = true;
                    $message = "warning', 'La taille du fichier est trop grande. la limite est de 2 mo";
                }
            }
        }

        return $array = array('erreur'=>$erreur,'errorMessage'=>$message);
    }

    function SubmitForm($olds,$session)
    {
        //dump($this->request);exit;
        $mycheckform = $this->myCheckForm();
        //dump($this->myCheckForm());exit;

        if ($this->form->isValid() && $mycheckform["erreur"] === false)
        {

            // mise à jour de la derniere date de modification
            $timezone = new \DateTimeZone('Europe/Paris');
            $sessionId = $session->getId();
            $this->trick->setLastModification(new \DateTime('now', $timezone));
            $this->trick->setLastUserModification($sessionId);
            $this->trick->setSlug($this->form->get("title")->getData());

            // videos
            $newsVideos = $this->editnewsVideos($this->form->get('videos')->getData(),$this->form->get('deleteOfVid')->getData(),$olds['olds_videos']);

            if(!empty($newsVideos))
            {
                $newsVideos = serialize($newsVideos);
                $this->trick->setVideos($newsVideos);
            }
            else
            {
                $this->trick->setVideos(NULL);
            }

            // cover
            if(!empty($this->form->get('cover')->getData()))
            {
                $newCover = $this->editCover($this->form->get('cover')->getData());
                $this->trick->setCover($newCover);
            }
            else
            {

                $newCover = $this->olds->getCover();
            }

            // on recupere la liste des images a supprimé que l'on transforme en tableau
            $newFilename = $this->editnewsImages($this->form->get('pictures')->getData(),$this->form->get('deleteOfPic')->getData(),$olds['olds_pictures']);

            //enregistre les nouvelles données pour les images supplementaires

            if(!empty($newFilename))
            {
                $listImages = serialize($newFilename);
                $this->trick->setPictures($listImages);
            }
            else
            {
                $this->trick->setPictures(NULL);
            }

            return true;
        }
        else{

            return false;
        }

    }

    public function editnewsVideos($videos, $deletevideo,$olds_videos)
    {
        if (!empty($videos))
        {
            $getNewsVideos = $videos;
            $newsVideos = explode(";", $getNewsVideos);
        }
        if (empty($videos))
        {
            $newsVideos = [];
        }


        // ETAPE 2 SIL Y A DES VIDEOS A SUPPRIMER ON COMPARE AVEC LES DONNEES DE LA TABLE CONTENANT LES ANCIENNES ET LES NOUVELLES VIDEOS


        if (!empty($deletevideo) || $deletevideo !== NULL)
        {
            $deleteVideos = $deletevideo;
            $VideosInDelete = explode(";", $deleteVideos);
        }
        else
        {
            $deleteVideos = [];
            $VideosInDelete = $deleteVideos;
        }

        // 3EME ETAPE SI LANCIENNE LISTE DE VIDEO N'est PAS VIDE ON INSERE LES DONNEES AVEC LES NOUVELLES

        if (!empty($olds_videos))
        {
            $convertOldVid = unserialize($olds_videos);

            foreach ($convertOldVid as $oldConvert)
            {
                if(!in_array($oldConvert,$VideosInDelete))
                {
                    array_push($newsVideos, $oldConvert);
                }
            }
        }
        return $newsVideos;
    }

    public function editCover($addcover)
    {

        $cover = $addcover; // recuperation des données get pictures du formulaire soumis
        $originalFilename1 = pathinfo($cover->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename1 = $this->slugger->slug($originalFilename1);
        $newCover = $safeFilename1 . '-' . uniqid() . '.' . $cover->guessExtension();

        // Move the file to the directory where brochures are stored

        $cover->move(
            $this->uploaddir,
            $newCover
        );

        return $newCover;

    }

    public function editnewsImages($addpictures,$tabOfDelete,$olds_pictures)
    {
        $files = $addpictures; // recuperation des données get pictures du formulaire soumis
        $deletePics = $tabOfDelete;
        $PicsInDelete = explode(";",$deletePics);
        $newFilename = [];


        // ajoute de nouvelles images
        if(!empty($files))
        {
            foreach ($files as $key => $file)
            {

                $originalFilename[$key] = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename[$key] = $this->slugger->slug($originalFilename[$key]);
                $newFilename[$key] = $safeFilename[$key] . '-' . uniqid() . '.' . $file->guessExtension();

                // Move the file to the directory where brochures are stored

                $file->move(
                    $this->uploaddir,
                    $newFilename[$key]
                );


                $Allfiles = [];
            }
        }

        foreach ($olds_pictures as $old_picture)
        {

            if (!in_array($old_picture, $PicsInDelete) && !empty($PicsInDelete))
            {
                array_push($newFilename, $old_picture);
            }
            else
            {
                $filesystem = new Filesystem();
                $filesystem->remove(getcwd().'\uploads\\'.$old_picture);
            }

        }

        return $newFilename;
    }


    public function getMediasIndatabse($pictures, $videos)
    {

        if(!empty($pictures) || $pictures !== NULL)
        {

            $olds_pictures = unserialize($pictures);

        }
        else
        {
            $olds_pictures = [];
        }

        // un tableau des anciennes videos
        if(!empty($videos) || $videos !== NULL)
        {
            $olds_videos = $videos;
            $olds_videosView = unserialize($videos);

        }
        else
        {
            $olds_videos = [];
            $olds_videosView = NULL;
        }

        $tab["olds_pictures"] = $olds_pictures;
        $tab["olds_videos"] = $olds_videos;
        $tab["olds_videosView"] = $olds_videosView;

        return $tab;
    }


}
