<?php


namespace App\Controller\handler\tricks;


class HandlerShowTrick
{
    private $form;
    private $request;
    private $slugger;
    private $validator;
    private $trick;
    private $uploaddir;
    private $olds;
    private $comment;
    private $user;

    public function setForm($form)
    {
        $this->form = $form;
    }

    public function getForm()
    {
        return $this->form;
    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setTrick($trick)
    {
        $this->trick = $trick;
    }

    public function getTrick()
    {
        return $this->trick;
    }

    public function setSlugger($slugger)
    {
        $this->slugger = $slugger;
    }

    public function getSlugger()
    {
        return $this->slugger;
    }

    public function setValidator($validator)
    {
        $this->slugger = $validator;
    }

    public function getValidator()
    {
        return $this->validator;
    }

    public function setUploadDir($uploaddir)
    {
        return $this->uploaddir = $uploaddir;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }
    public function setUser($user)
    {
        $this->user = $user;
    }

    public function __construct()
    {

    }

    public function getAllMedias()
    {
        //dump($this->trick);exit;
        if(!empty($this->trick[0]->getPictures()))
        {
            $allPictures = unserialize($this->trick[0]->getPictures());
        }

        if(empty($this->trick[0]->getPictures()))
        {
            $allPictures = [];
        }

        // recuperation des videos
        if(!empty($this->trick[0]->getVideos()))
        {
            $allVideos = unserialize($this->trick[0]->getVideos());
        }

        if(empty($this->trick[0]->getVideos()))
        {
            $allVideos = [];
        }

        return array("allPictures"=>$allPictures,"allVideos"=>$allVideos);
    }


}
