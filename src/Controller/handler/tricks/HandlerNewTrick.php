<?php


namespace App\Controller\handler\tricks;


use App\Controller\TricksController;
use App\Entity\CategoryRef;
use App\Entity\Tricks;
use App\Entity\User;
use App\Form\TricksType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Routing\Annotation\Route;


class HandlerNewTrick extends AbstractController
{

    private $form;
    private $request;
    private $slugger;
    private $validator;
    private $user;
    private $trick;
    private $uploaddir;


    public function __construct()
    {
        $this->user = new User();
    }


    public function setForm($form)
    {
        $this->form = $form;
    }

    public function getForm()
    {
        return $this->form;
    }

    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function setTrick($trick)
    {
        $this->trick = $trick;
    }

    public function getTrick()
    {
        return $this->trick;
    }

    public function setSlugger($slugger)
    {
        $this->slugger = $slugger;
    }

    public function getSlugger()
    {
        return $this->slugger;
    }

    public function setValidator($validator)
    {
        $this->slugger = $validator;
    }

    public function getValidator()
    {
        return $this->validator;
    }

    public function setUploadDir($uploaddir)
    {
        return $this->uploaddir = $uploaddir;
    }


    public function myCheckForm()
    {
            $erreur = false;
            $message = "";
            $extensions = array("jpg","png","jpeg");
            $verifCover = $this->form->get('cover')->getData();
            $pictures = $this->form->get('pictures')->getData();


            if(empty($verifCover))
            {
                $erreur = true;
                $message = "L'image pour la figure est vide. Veuillez selectionnez une image";
            }

            if(!empty($verifCover) || $verifCover !== null)
            {

                if(!in_array(strtolower($verifCover->guessExtension()),$extensions))
                {
                    $erreur = true;
                    $message ="L'extension n'est pas valide. veuillez choisir une extension valide parmi : png, jpg, jpeg";
                }

                if($verifCover->getSize() > 2000000)
                {
                    $erreur = true;
                    $message = "warning', 'La taille du fichier est trop grande. la limite est de 2 mo";
                }
            }

            foreach ($pictures as $file)
            {
                if(!in_array(strtolower($file->guessExtension()),$extensions))
                {
                    $erreur = true;
                    $message ="Une extension n'est pas valide. veuillez choisir une extension valide parmi : png, jpg, jpeg";
                }
                if($file->getSize() > 2000000)
                {
                    $erreur = true;
                    $message = "warning', 'La taille du fichier est trop grande. la limite est de 2 mo";
                }
            }

            /*if(!empty($this->form->get('videos')->getData()))
            {
                $videos = $this->form->get('videos')->getData();
                $videos = explode(";",$videos);

                foreach ($videos as $video)
                {
                    if(!preg_match('/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/\s]{11})/i',$video))
                    {
                        $erreur = true;
                        $message = "Une video n'est pas au bon format";
                    }
                }
            }*/



        return $array = array('erreur'=>$erreur,'errorMessage'=>$message);
    }

    function SubmitForm($session)
    {
        $mycheckform = $this->myCheckForm();

        if ($this->form->isValid() && $mycheckform["erreur"] === false)
        {
            $timezone = new \DateTimeZone('Europe/Paris');
            $this->trick->setLastModification(new \DateTime('now', $timezone));
            $this->trick->setDateC(new \DateTime('now', $timezone));
            $this->trick->setLastUserModification($session->getId());
            $this->trick->setFkAuthor($session);
            $this->trick->setSlug($this->form->get("title")->getData());


            // upload videos
            $getVideos = $this->form->get('videos')->getData();
            if(!empty($this->form->get('videos')->getData()))
            {
                $exVideos = explode(";",$getVideos);
                $Videos = serialize($exVideos);
                $this->trick->setVideos($Videos);
            }
            else
            {
                $this->trick->setVideos(NULL);
            }

            // cover

            $newCover = $this->addnewCover($this->form->get('cover')->getData());
            $this->trick->setCover($newCover);

            // ================= ajout des nouvelles images

            $newFilename = $this->addnewPictures($this->form->get('pictures')->getData());
            // set images

            if(!empty($newFilename))
            {
                $listImages = serialize($newFilename);
                $this->trick->setPictures($listImages);
            }
            else
            {
                $listImages = NULL;
                $this->trick->setPictures($listImages);
                //$newFilename = serialize($newFilename);
            }

            return true;

        }else
        {
            return false;
        }

    }


    public function addnewPictures($pictures)
    {
        if(!empty($pictures))
        {

            $files = $pictures; // recuperation des données get pictures du formulaire soumis

            //dd($files);
            foreach ($files as $key => $file)
            {

                $originalFilename[$key] = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename[$key] = $this->slugger->slug($originalFilename[$key]);
                $newFilename[$key] = $safeFilename[$key] . '-' . uniqid() . '.' . $file->guessExtension();

                // Move the file to the directory where brochures are stored
                try
                {
                    $file->move(
                        $this->uploaddir,
                        $newFilename[$key]
                    );
                }
                catch (FileException $e)
                {
                    // ... handle exception if something happens during file upload
                }

                $Allfiles = [];

            }
            return $newFilename;
        }
    }
//======= class a pars ==============
    private function addnewCover($addcover)
    {
        // cover
        $cover = $addcover; // recuperation des données get pictures du formulaire soumis

        //dump($this->slugger);exit;
        $originalFilename1 = pathinfo($cover->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename1 = $this->slugger->slug($originalFilename1);
        $newCover = $safeFilename1.'-'.uniqid().'.'.$cover->guessExtension();

        // Move the file to the directory where brochures are stored
        try
        {
            $cover->move(
                $this->uploaddir,
                $newCover
            );
        }
        catch (FileException $e)
        {
            // ... handle exception if something happens during file upload
        }

        return $newCover;
    }


}
