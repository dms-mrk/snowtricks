<?php

namespace App\Controller;

use App\Entity\Tricks;
use App\Repository\TricksRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class HomeController extends AbstractController
{

    /**
     * @Route("/", name="home")
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function index(Request $request, PaginatorInterface $paginator, MailerInterface $mailer)
    {

        $gettricks = $this->getDoctrine()->getRepository(Tricks::class);
        //$tricks = $gettricks->findBy(array(),array('id' => 'DESC'));

        $allTricks = $gettricks->findBy([],['last_modification'=>'DESC']);

        $tricks = $paginator->paginate(
            $allTricks, // on passe les données
            $request->query->getInt('page',1), // numero de la page en cours par défaut
            9 // nombre d'élement par page
        );
        $tricks->setTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_pagination.html.twig');
        $tricks->setSortableTemplate('@KnpPaginator/Pagination/twitter_bootstrap_v4_font_awesome_sortable_link.html.twig');

        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController', 'tricks'=>$tricks
        ]);
    }
}
