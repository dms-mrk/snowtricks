<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryRefController extends AbstractController
{
    /**
     * @Route("/category/ref", name="category_ref")
     */
    public function index()
    {
        return $this->render('category_ref/index.html.twig', [
            'controller_name' => 'CategoryRefController',
        ]);
    }
}
